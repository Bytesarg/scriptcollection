#!/usr/bin/perl -w

use strict;
use Net::Ping;
use Net::IP;
use Socket;

if (!$ARGV[0]){
	print "IP + Subnetmask ist required eg: 123.456.789.0/24\n";
	exit(1);
}

my $ip = Net::IP->new($ARGV[0]);
my $ping = Net::Ping->new();
do{
	my $address = $ip->ip();
	my @return = `/usr/bin/host $address`;
	chomp @return;
	
	if($?==0){
		my $keys = @return;
		my $fault = 0;
		my $host;
		if($keys > 1){
			print "IP: $address has multiple PTR-Records\n";
			$fault = 1;
		}
		foreach(@return){
			my $index = rindex($_, " ");
			$host = substr($_, $index+1,-1);
			my $forward = `/usr/bin/host $host`;
			if($?==0){
				chomp $forward;
				my $findex = rindex($forward," ");
				$forward = substr($forward, $findex+1);
				if($forward){
					if($forward ne $address){
						print "IP: $address ($host) A-Record-Error: $forward does not match\n";
						$fault = 3;
					}
				}
			}else{
				print "IP: $address ($host) No A-Record found\n";
				$fault = 4;
			}
			if($fault == 1){
				print "IP: $address = $host\n";
			}
			if($fault == 2){
				print "IP: $address = $host --> Resolve-Error (DNS-Return: $forward)\n";
			}
		}
		if($ping->ping($address)){
		}else{
			print "IP: $address ($host) has PTR / A-Records but is down\n\n";
			$fault = 0;
		}
		if($fault){
			print "\n";
		}
	}
}while(++$ip);
$ping->close();
